%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% [RB,Vh,v] = POD(Uh, threshold, Xh, varargin)
%
% This function computes the Proper Orthogonal Decomposition of a data set
% with respect to several options.
% By default, the spectrum of the correlation matrix is estimate using a 
% QR method to find the compression criterion. Then the couples eigen
% val.-vect. are found using power method.
%
%  input: - Uh: snapshot data set to reduce
%         - threshold: compression threshold
%         - Xh: inner product matrix
%         - varargin: set of options
%            'full'     => compute the full SVD, adapted for small data set
%            'absolute' => the threshold corresponds to the proj. error
%            'relative' => the threshold corresponds to relative proj. error
%
% output: - RB: the built reduced basis
%         - Vh: RB decomposition with respect to the data set
%         - v: computed matrix spectrum
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [RB,Vh,v] = POD(Uh, threshold, Xh, varargin)
  
  % --- Opts
  % Default
  pod_order = 'large';
  pod_norm  = 'relative';
  % Check
  for i = 1:length(varargin)
    if strcmpi(varargin{i},'full')
      pod_order = 'full';
    end
    if strcmpi(varargin{i},'absolute')
      pod_norm  = 'absolute';
    end
  end
  
  % --- Adapt threshold in relative case
  if strcmpi('relative',pod_norm)
    Unorm = zeros(size(Uh,2),1);
    for i = 1:size(Uh,2)
      Unorm(i) = sqrt(Uh(:,i)'*Xh*Uh(:,i));
    end
    threshold = threshold * min(Unorm);
  end
  
  % --- Correlation
  C = Uh'*Xh*Uh;
  
  % ------ Using standard SVD for small sized dataset
  if strcmpi(pod_order,'full')
    % --- Eigen solve
    [U,S,V] = svd(C);
    v = diag(S);
    
    % --- Stop criterion
    N = sum( sqrt( cumsum(flip(v))./size(Uh,2) ) >= threshold );
    
    % --- Reduced Basis
    Vh = U(:,1:N)./sqrt(v(1:N))';
    RB = Uh*Vh;
  
  % ------ Using progressive power method for large sized dataset
  else
    % --- Spectrum estimate and stop criterion
%     v = spectrum_estimate(C,1e-2,20);
    v = sort(eig((C'+C).*0.5),'descend'); % force call to eig in symm mode and only spectr.
    N = sum( sqrt( cumsum(flip(v))./size(Uh,2) )  >= threshold );
    
    % --- Singular Value Decomposition
    [U,u] = eigs(C,N);
    
    % --- Reduced Basis
    Vh = U./(sqrt(diag(u))');
    RB = Uh*Vh;
    %RB = GS(Uh*Vh,Xh);
  end
  
  
end