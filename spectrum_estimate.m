%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% [S,k,res] = spectrum_estimate(A, threshold, kmax)
%
% This function computes an estimate of the whole matrix spectrum.
%
%  input: - A: snapshot data set to reduce
%         - threshold: estimate threshold
%         - kmax: iteration limit [optional, default=200]
%
% output: - S: estimate spectrum
%         - k: number of iterations
%         - res: max non-diagonal value
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [S,k,res] = spectrum_estimate(A, threshold, kmax)
  
  % --- Opts
  if exist('kmax','var')==0
    kmax = 200;
  end
  
  % --- Initiate
  err = threshold+1;
  B   = hess(A);
  B   = sparse(diag(diag(B,1),1) + diag(diag(B)) + diag(diag(B,-1),-1));
  k   = 0;
  tmp = 0;
  
  % --- Main loop
  while err>threshold && k<kmax
    [Q,R] = qr(B);
    B = R*sparse(Q);
    B   = sparse(diag(diag(B,1),1) + diag(diag(B)) + diag(diag(B,-1),-1));
    res = max(max(abs(triu(B,1))));
    err = abs(tmp - mean(abs(diag(R))));
    tmp =  mean(abs(diag(R)));
    k = k+1;
  end
  
  % --- Sorting
  S = sort(abs(diag(R)),'descend');
end